# OpenAI

Repositorio para alojar scripts sobre el curso de ChatGPT.

Curso en https://www.deeplearning.ai/short-courses/chatgpt-prompt-engineering-for-developers/


## Contenido del repositorio

    |- README.md
    |- guidelines.py    [Ejemplo básico]
    |- iterative.py	 [Ejemplo en el que chatGPT devuelve un html, que luego es renderizado en un server Flask]
    |- summarizing.py   []

## Docs

La documentación de la API en python y javascript 
 - https://platform.openai.com/docs/api-reference
 
## Otros

 - Cuando se realizan varias peticiones en bucle, aparece el siguiente prompt. Ya que hay limitaciones en la cuenta gratuita.
 
 ```
 Traceback (most recent call last):
  File "summarizing.py", line 110, in <module>
    response = get_completion(prompt)
  File "summarizing.py", line 11, in get_completion
    response = openai.ChatCompletion.create(
  File "/home/circe/.local/lib/python3.8/site-packages/openai/api_resources/chat_completion.py", line 25, in create
    return super().create(*args, **kwargs)
  File "/home/circe/.local/lib/python3.8/site-packages/openai/api_resources/abstract/engine_api_resource.py", line 153, in create
    response, _, api_key = requestor.request(
  File "/home/circe/.local/lib/python3.8/site-packages/openai/api_requestor.py", line 230, in request
    resp, got_stream = self._interpret_response(result, stream)
  File "/home/circe/.local/lib/python3.8/site-packages/openai/api_requestor.py", line 624, in _interpret_response
    self._interpret_response_line(
  File "/home/circe/.local/lib/python3.8/site-packages/openai/api_requestor.py", line 687, in _interpret_response_line
    raise self.handle_error_response(
openai.error.RateLimitError: Rate limit reached for default-gpt-3.5-turbo in organization org-NBnsejBQlwoSg3OddKdTol6S on requests per min. Limit: 3 / min. Please try again in 20s. Contact us through our help center at help.openai.com if you continue to have issues. Please add a payment method to your account to increase your rate limit. Visit https://platform.openai.com/account/billing to add a payment method.
 ```
